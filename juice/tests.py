'''
test suite for the competition service.
'''
import random

from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework.authtoken.models import Token

from juice.models import User, Competition, Competitor

def gen_str(n):
        return ''.join([ chr(random.randint(65,98)) for i in range(n) ])
def gen_num(n):
        return ''.join([ str( random.randint(0,9) ) for i in range(n) ])

class UserTest(TestCase):
	def setUp(self):
		User.objects.create(username="somebody", email="random@email.com", phone_number="3037209876")

# NOTE: this is the important part as you will put in your different test cases and make sure that each one works.
# TODO: you need to remove the User object you declared and use the given auth user instead.
class CompetitionTest(TestCase):
	def setUp(self):
		# mocking a user logging in/ otherwise the tests would result in 302's
		token = Token.objects.get(user__username='lauren')
		client = APIClient()
		client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
		client.login(username='jrrtolkien', password='peachesandcream')
		# create a bunch of users for the system
		User.objects.create(username="jimi hendrix", email="random@email.com", phone_number="3037209876")
		User.objects.create(username="janis joplin", email=gen_str(64), phone_number=gen_num(10))
		User.objects.create(username="frank zappa", email=gen_str(64), phone_number=gen_num(10))
		User.objects.create(username="skrillex", email=gen_str(64), phone_number=gen_num(10))
		User.objects.create(username="porter robinson", email=gen_str(64), phone_number=gen_num(10))
		User.objects.create(username="major lazer", email=gen_str(64), phone_number=gen_num(10))
		User.objects.create(username="noisia", email=gen_str(64), phone_number=gen_num(10))
		# create some competitions for users to enroll in
		Competition.objects.create(name="the pepsi challenge")
		Competition.objects.create(name="soccer goal challenge")
		Competition.objects.create(name="mma soccer fusion challenge")
		Competition.objects.create(name="magic the gathering")
		Competition.objects.create(name="pokemon go")
		# 

	def tearDown(self):
		client.logout()

	def test_user_competitor_single_time(self):
		''' a single user becomes a competitor. '''
		user = User.objects.get(username="jimi hendrix")

	def test_single_competitor_doesnt_already_exist(self):
		pass

	def test_single_competitor_already_exist(self):
		# Should return an error string saying the user already exists. validate that there is still only one competitor with thta user id.
		pass