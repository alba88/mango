from django.contrib import admin

from models import User, Competition, Competitor

admin.site.register(User)
admin.site.register(Competition)
admin.site.register(Competitor)
