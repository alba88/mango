import json
from pprint import pprint

from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404
from django.views import View
from django.forms.models import model_to_dict
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import authenticate

#from django.utils import simplejson
from rest_framework.generics import GenericAPIView

################ HACKY CSRF SKIPPING ##################
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
################ HACKY CSRF SKIPPING ##################

from models import User, Competition, Competitor, VotingRecord

@csrf_exempt
def register(request, competitor_id, competition_id):
	# TODO: basically this only needs to check if the user has already been registered. if not register that user as a competitor in that competition.
	is_registered = Competitor.objects.filter(id=competitor_id, competition_id=competition_id)
	if len(is_registered) == 0:
		new_competitor = Competitor.objects.filter(id=competitor_id)
		new_competitor.competition_id = competition_id
		new_competitor.save()
		return JsonResponse({"success": True})
	else:
		return JsonResponse({"success": False, "reason": "competitor is already registered for that competition."})

@csrf_exempt
def login(request):
	j = json.loads(request.body)
	user = authenticate(username=j['username'], password=j['password'])
	if user is not None:
		# A backend authenticated the credentials
		print("authenticated user %s" % j['username'])
		return JsonResponse({"success": True})
	else:
		# No backend authenticated the credentials
		print("did not authenticate user %s" % j['username'])
		return JsonResponse({"success": False, "reason": "you have entered in incorrect user/password information."})

# TODO: you have to come up with a better way to do voting. also look at the pdf as it is very well detailed into how all of these things should interact within the app.
@csrf_exempt
def vote(request, competitor_id, competition_id, user_id):
	'''
	handles a competitor vote.
	'''
	# NOTE: when a competitor joins a competition there should be a short url generated to give to the other user to handle voting.
	# 1.) check if competitor is regestered for competition
	if len(Competitor.objects.filter(id=competitor_id, competition_id=competition_id)) == 0:
		return JsonResponse({"success": False, "reason":"that competitor is not registered for that competition."})
	# check ifa vote has already been made for that competition with a different competitor
	voted_before = VotingRecord.objects.filter(competition_id=competition_id, user_id=user_id)
	# TODO: might need to handle the logic for checking the array amount for `voted_before`
	if len(voted_before) > 0:
		good_competitor = False
		# TODO: check this logic as I think its an incorrect query.
		old_competitor = Competitor.objects.filter(competition_id=competition_id, id=voted_before[0].competitor_id)
		if len(old_competitor) == 1:
			old_competitor = old_competitor[0]
			good_competitor = True
		elif len(old_competitor) == 0:
			# TODO: you have an inconsistency as there is a voting record, but there is no coresponding competitor. better delete the voting record and apply the vote to the current competitor
			# delete the voting record
			# recursively call this function again.
			pass
		elif len(old_competitor) > 1:
			return JsonResponse({"success": False, "reason":"you have a user registered for multiple competitors in a competitoin."})
		# do nothing with the database if this is the same user vote for the same competitor.
		if good_competitor and old_competitor.id == competitor_id:
			return JsonResponse({"success": False, "reason": "a vote for that competitor in that competiton from that user has already been made."})
		# XXX: the below functionality will cause a bug if the entire process does not finish. you could have a missing vote in space.
		old_competitor.votes -= 1
		old_competitor.save()
		# update_competitors(old_competitor.competition_id)
		old_voting_record = VotingRecord.objects.filter(competition_id=competition_id, user_id=user_id, competitor_id=old_competitor.id)
		old_voting_record.delete() # TODO: make sure this does what you want it to do.
		new_competitor = Competitor.objects.get(id=competitor_id)
		new_competitor.votes += 1
		new_competitor.save()
		update_competitors(new_competitor.competition_id)
		new_voting_record = VotingRecord(competitor=Competitor.objects.get(id=competitor_id), \
										 competition=Competition.objects.get(id=competition_id), \
										 user=User.objects.get(id=user_id) )
		new_voting_record.save()
		return JsonResponse({"success": True})
	# 2.) check if voting record has already been created.
	already_voted = VotingRecord.objects.filter(competitor_id=competitor_id, competition_id=competition_id, user_id=user_id)
	if len(already_voted) > 1:
		# TODO: implement some logic to handle this scenario as it means you have a broken system
		return JsonResponse({"success": False, "reason": "there are corrupt entries in the database that need to be fixed."})
	if len(already_voted) == 1:
		# TODO: check to see if its a duplicate vote.
		return JsonResponse({"success": False, "reason": "a vote for that competitor in that competiton from that user has already been made."})
	if len(already_voted) == 0:
		# TODO: handle the basic logic for inserting a voting record for this transaction.
		new_voting_record = VotingRecord(competitor=Competitor.objects.get(id=competitor_id), \
										 competition=Competition.objects.get(id=competition_id), \
										 user=User.objects.get(id=user_id) )
		new_voting_record.save()
		# TODO: update any tabls with vote information. meaning competitor being voted for gets a +1 vote.
		this_competitor = Competitor.objects.get(id=competitor_id)
		this_competitor.votes += 1
		this_competitor.save()
		print("updating the competitors")
		update_competitors(this_competitor.competition_id)
		print("updating the competitors done............")
		return JsonResponse({"success": True})

# NOTE: will not add a standing for the item being voted on.
def update_competitors(competition_id):
	'''
	method that maintains the competition_standing attribute for all rows.
	'''
	# TODO: you need to figure out how to not increment n if the next value == the current value.
	competitors = Competitor.objects.filter(competition=competition_id).order_by('-votes') # '-votes' == descending order.
	#competitors = Competitor.objects.filter(competition=competition_id)
	l = []
	n = 0
	competitor_position = 0
	for competitor in range(len(competitors)):
		if len(competitors) > competitor_position + 1 and competitor_position != 0:  # check array bounds.
			if competitors[competitor].votes == competitors[competitor-1].votes:
				competitors[competitor].competition_standing = n
				l.append("Competitor[%s]: { votes: %s , standing: %s }" % (str(competitors[competitor].id), str(competitors[competitor].votes), str(competitors[competitor].competition_standing) ) )
			else:
				n += 1
				competitors[competitor].competition_standing = n
				l.append("Competitor[%s]: { votes: %s , standing: %s }" % (str(competitors[competitor].id), str(competitors[competitor].votes), str(competitors[competitor].competition_standing) ) )
		else:
			n += 1
			competitors[competitor].competition_standing = n
			l.append("Competitor[%s]: { votes: %s , standing: %s }" % (str(competitors[competitor].id), str(competitors[competitor].votes), str(competitors[competitor].competition_standing) ) )
		# TODO: write an edge case scenario handler for the last 2 items because the current logic is incorrect.
		competitor_position += 1
		competitors[competitor].save()
	# NOTE: hacky fix for the final 2 competitor resources
	if len(competitors) > 1:
		last_competitor = competitors[len(competitors) - 1 ]
		second_last_competitor = competitors[len(competitors) - 2 ]
		if last_competitor.votes == second_last_competitor.votes:
			last_competitor.competition_standing = second_last_competitor.competition_standing
			last_competitor.save()
	print('\n'.join(l))

# TODO: return the results of a competition.
@method_decorator(csrf_exempt, name='dispatch')
class CompetitionView(LoginRequiredMixin, View): #GenericAPIView): # LoginRequiredMixin
	def get(self, request, competition_id):
		# TODO: use the sort function from the model to return the list of top competitors.
		data = get_object_or_404(Competition, id=competition_id ) 
		data = model_to_dict(data)
		competitors = [ model_to_dict(competitor) for competitor in Competitor.objects.filter(competition_id=competition_id) ]
		data['competitors'] = competitors
		#data = model_to_dict(data)
		return JsonResponse(data)

	def post(self, request):
		j = json.loads(request.body)
		pprint(j)
		if 'name' not in j:
			return JsonResponse({"success": False, "reason": "you need to specify [`name`] in your json."})
		# TODO: check if a competition already exist within the code.
		name = j['name'] #request.POST.get('name','')
		new_competition = Competition(name=name)
		new_competition.save()
		return JsonResponse({"success": True})

	def delete(self, request, competition_id):
		competition = get_object_or_404(Competition, id=competition_id)
		if competition:
			competition.delete()
			return JsonResponse({"success": True})
		else:
			JsonResponse({"success": False})

	def put(self, request, competition_id):
		pass

@method_decorator(csrf_exempt, name='dispatch')
class UserView(View): #GenericAPIView):
	def get(self, request, user_id):
		data = get_object_or_404(User, id=user_id ) 
		data = model_to_dict(data)
		return JsonResponse(data)

	def post(self, request):
		# TODO: test this simple method first to make sure that everything is working.
		# TODO: validate that this is checking for if the required fields exist.
		j = json.loads(request.body)
		pprint(j)
		if 'username' not in j or 'email' not in j or 'phone_number' not in j:
			return JsonResponse({"success": False, "reason": "you need to specify [`username`,`email`,`phone_number`] values in your json..."})
		username = j['username']
		email = j['email']
		phone_number = j['phone_number']
		new_user = User(username=username, email=email, phone_number=phone_number)
		new_user.save()
		return JsonResponse({"success": True})

	def delete(self, request, user_id):
		user = get_object_or_404(User, id=user_id)
		if user:
			user.delete()
			return JsonResponse({"success": True})
		else:
			JsonResponse({"success": False})

	def put(self, request, user_id):
		j = json.loads(request.body)
		user = get_object_or_404(User, id=user_id)
		if "username" in j:
			user.username = j["username"]
		if "email" in j:
			user.email = j["email"]
		if "phone_number" in j:
			user.phone_number = j["phone_number"]
		user.save()
		return JsonResponse({"success": True})


@method_decorator(csrf_exempt, name='dispatch')
class CompetitorView( View): #GenericAPIView): # LoginRequiredMixin
	def get(self, request, competitor_id):
		data = get_object_or_404(Competitor, id=competitor_id ) 
		data = model_to_dict(data)
		return JsonResponse(data)

	def post(self, request):
		j = json.loads(request.body)
		print("entering the matrix")
		user = j['user'] #request.POST.get('user') # unique int to the user whos competing.
		# NOTE: check to see if the user already exists in the competition. if he does do not add him and send a reminder.
		is_user = Competitor.objects.filter(user=user)
		if len(is_user) > 0:
			print("you just tried to register a user that is already a competitor in that competition.")
			return JsonResponse({"success": False, "reason": "User already is enrolled in competition."})
		competition = j['competition'] #request.POST.get('competition', '') # unique int tothe competition being held.
		votes = 0 # j['votes'] #request.POST.get('votes', '')
		this_user = User.objects.get(id=user)
		this_competition = Competition.objects.get(id=competition)
		new_competitor = Competitor(user=this_user, competition=this_competition, votes=votes)
		new_competitor.save()
		self.update_competitors(competition)
		return JsonResponse({"success": True})

	def delete(self, request, competitor_id):
		competitor = get_object_or_404(Competitor, id=competitor_id)
		if competitor:
			competitor.delete()
			return JsonResponse({"success": True})

	def put(self, request, competitor_id):
		#j = json.loads(request.body)
		#competitor = get_object_or_404(Competitor, id=competitor_id)
		# NOTE: user should not be able to change the competition type
		return JsonResponse({"success": False, "reason": "you can not update a competitor. you must delete the competitor and then register for the competitoin you want to join."})

	def vote(self, request, competitor_id, competition_id, user_id):
		'''
		handles a competitor vote.
		'''
		# NOTE: when a competitor joins a competition there should be a short url generated to give to the other user to handle voting.
		# 1.) check the votingrecords for a entry already made.
		print("helo")
		already_voted = VotingRecord.objects.filter(competitor_id=competitor_id, competition_id=competition_id)
		if len(already_voted) > 1:
			# TODO: implement some logic to handle this scenario as it means you have a broken system
			return JsonResponse({"success": False, "reason": "there are corrupt entries in the database that need to be fixed."})
		if len(already_voted) == 1:
			return JsonResponse({"success": False, "reason": "a vote for that competitor in that competiton from that user has already been made."})
		if len(already_voted) == 0:
			# TODO: handle the basic logic for inserting a voting record for this transaction.
			new_voting_record = VotingRecord(competitor=Competitor.objects.get(id=competitor_id), \
											 competition=Competition.objects.get(id=competition_id), \
											 user=User.objects.get(id=user_id) )
			new_voting_record.save()
			# TODO: update any tabls with vote information. meaning competitor being voted for gets a +1 vote.
			this_competitor = Competitor.objects.get(id=competitor_id)
			print("updating the competitors")
			self.update_competitors(this_competitor.competition_id)
			print("updating the competitors done............")
			this_competitor.votes += 1
			this_competitor.save()
			return JsonResponse({"success": True})

# XXX: you need to test all the use cases of this.
	def update_competitors(self, competition_id):
		'''
		method that maintains the competition_standing attribute for all rows.
		'''
		# TODO: you need to figure out how to not increment n if the next value == the current value.
		competitors = Competitor.objects.filter(competition=competition_id).order_by('-votes') # '-votes' == descending order.
		#competitors = Competitor.objects.filter(competition=competition_id)
		l = []
		n = 0
		competitor_position = 0
		for competitor in range(len(competitors)):
			if len(competitors) > competitor_position + 1 and competitor_position != 0:  # check array bounds.
				if competitors[competitor].votes == competitors[competitor-1].votes:
					competitors[competitor].competition_standing = n
					l.append("Competitor[%s]: { votes: %s , standing: %s }" % (str(competitors[competitor].id), str(competitors[competitor].votes), str(competitors[competitor].competition_standing) ) )
				else:
					n += 1
					competitors[competitor].competition_standing = n
					l.append("Competitor[%s]: { votes: %s , standing: %s }" % (str(competitors[competitor].id), str(competitors[competitor].votes), str(competitors[competitor].competition_standing) ) )
			else:
				n += 1
				competitors[competitor].competition_standing = n
				l.append("Competitor[%s]: { votes: %s , standing: %s }" % (str(competitors[competitor].id), str(competitors[competitor].votes), str(competitors[competitor].competition_standing) ) )
			# TODO: write an edge case scenario handler for the last 2 items because the current logic is incorrect.
			competitor_position += 1
			competitors[competitor].save()
		# NOTE: hacky fix for the final 2 competitor resources
		if len(competitors) > 1:
			last_competitor = competitors[len(competitors) - 1 ]
			second_last_competitor = competitors[len(competitors) - 2 ]
			if last_competitor.votes == second_last_competitor.votes:
				last_competitor.competition_standing = second_last_competitor.competition_standing
				last_competitor.save()
		print('\n'.join(l))
