from django.db import models
from django.contrib.postgres.fields import ArrayField

import json

# XXX: this is a mock of a user. this will have to be edited down the road.
class User(models.Model):
    '''
    mock user for the competition
    '''
    id = models.AutoField(primary_key=True)
    # id = models.IntegerField(primary_key=True, db_column='uid')
    username = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=100)
    created_at = models.DateTimeField(verbose_name=('Created At'),
        auto_now_add=True, help_text=("Date when submission history created."))
    updated_at = models.DateTimeField(verbose_name=('Updated At'),
        auto_now=True, help_text=("Date when submission history updated."))
    deleted_at = models.DateTimeField(verbose_name=('Deleted At'),
        default=None, null=True, blank=True, help_text=("Date when submission history deleted."))

    #class Meta:
    #    managed = False
    #    db_table = 'bnn_users'

    def to_string(self):
        return "user\n\tid=%s\n\t" % (str(self.id))

class Competition(models.Model):
    '''
    a competition is a table that houses any promotional competitions. Each row will signify a different competition.
    '''
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=256, default="Basic Competition")
    # id = models.IntegerField(primary_key=True, db_column='uid')
    # TODO: you might need to make this a one to many relation.
    # competitors =  # SeparatedValuesField() # ArrayField(models.ForeignKey(User))
    created_at = models.DateTimeField(verbose_name=('Created At'),
        auto_now_add=True, help_text=("Date when submission history created."))
    updated_at = models.DateTimeField(verbose_name=('Updated At'),
        auto_now=True, help_text=("Date when submission history updated."))
    deleted_at = models.DateTimeField(verbose_name=('Deleted At'),
        default=None, null=True, blank=True, help_text=("Date when submission history deleted."))

    #class Meta:
    #    managed = False
    #    db_table = 'bnn_competition'

    def return_competition_results(self, n=100):
        # TODO: generate a list of n users that have the top rankings in a promotion competition.
        sub_competitors = self.competitors

    def add_competitor(self, competitior):
        self.competitors

class Competitor(models.Model):
    '''
    a Competitor row that houses the info for voting competition ranking.
    '''
    # TODO: change all of these into the appropriate attributes. 
    id = models.AutoField(primary_key=True)
    # id = models.IntegerField(primary_key=True, db_column='uid')
    user = models.ForeignKey(User, blank=True, null=True)
    competition = models.ForeignKey(Competition, blank=True, null=True)
    votes = models.IntegerField(default=0)
    competition_standing = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(verbose_name=('Created At'),
        auto_now_add=True, help_text=("Date when submission history created."))
    updated_at = models.DateTimeField(verbose_name=('Updated At'),
        auto_now=True, help_text=("Date when submission history updated."))
    deleted_at = models.DateTimeField(verbose_name=('Deleted At'),
        default=None, null=True, blank=True, help_text=("Date when submission history deleted."))
    #objects = UserManager()

    class Meta:
        unique_together = ('user','competition',)
    #class Meta:
    #    managed = False
    #    db_table = 'bnn_competitors'

    def generate_voting_url(self):
        '''
        return a url to vote for the competitor.
        '''
        # NOTE: url should be generated from the settings file. as well as this should be sitting behind a https proxy.
        url = "http://localhost:8000"
        competition_id = self.competition.id
        competitor_id = self.id
        # user_id = self.user.id # NOTE: user_id needs to be the user voting for the competitor.
        # "vote/competition/(?P<competition_id>[0-9]+)/competitor/(?P<competitor_id>[0-9]+)/user/(?P<user_id>[0-9]+)/"
        return "%s/vote/competition/%s/competitor/%s/user/" % (url, competition_id, competitor_id)

    def add_vote(self):
        self.votes += 1

    def to_json(self):
        return json.dumps(self.__dict__)

class VotingRecord(models.Model):
    '''
    voting_record - a table for ensuring that one user can vote for one competitor per competition.
    '''
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, blank=False, null=False)
    competitor = models.ForeignKey(Competitor, blank=False, null=False)
    competition = models.ForeignKey(Competition, blank=False, null=False)
    created_at = models.DateTimeField(verbose_name=('Created At'),
        auto_now_add=True, help_text=("Date when submission history created."))
    deleted_at = models.DateTimeField(verbose_name=('Deleted At'),
        default=None, null=True, blank=True, help_text=("Date when submission history deleted."))

    class Meta:
        unique_together = ('user','competition','competitor',)

    def __str__(self):
        return "Voting Record[%s] =  { user: %s, competitor: %s, competition: %s }" % (str(self.id), str(self.user.id), str(self.competitor.id), str(self.competition.id))
