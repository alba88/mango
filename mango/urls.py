"""mango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from juice.views import CompetitionView, UserView, CompetitorView, login, vote, register

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^add_user/', UserView.as_view()),
    url(r'^competition/(?P<competition_id>[0-9]+)/$', CompetitionView.as_view()),
    url(r'^competition/$', CompetitionView.as_view()), # POST
    url(r'^competitor/(?P<competitor_id>[0-9]+)/$', CompetitorView.as_view()),
    url(r'^competitor/$', CompetitorView.as_view()), # POST
    url(r'^user/(?P<user_id>[0-9]+)/$', UserView.as_view()),
    url(r'^user/$', UserView.as_view()), # POST
    url(r'^login/$', login),
    url(r'^vote/competition/(?P<competition_id>[0-9]+)/competitor/(?P<competitor_id>[0-9]+)/user/(?P<user_id>[0-9]+)/$', vote),
    url(r'^register/competitor/(?P<competitor_id>[0-9]+)/competition/(?P<competition_id>[0-9]+)/$', register), # TODO: implement this functionality.
]
