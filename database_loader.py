# coding: utf-8
'''
this is a simple database loading script for the competition functionality.
'''
import random
from juice.models import *

def gen_str(n):
        return ''.join([ chr(random.randint(65,98)) for i in range(n) ])

def gen_num(n):
        return ''.join([ str( random.randint(0,9) ) for i in range(n) ])

# =========================
# Amount of users to create
# =========================
user_amount = 666
competition_amount = 10
competitor_amount = 200


# =========================
#    begin script logic
# =========================
u = User(username=gen_str(99), email=gen_str(64), phone_number=gen_num(10))
u.save()
user_idi = int(u.id)
for i in range(user_amount - 1):
    u = User(username=gen_str(99), email=gen_str(64), phone_number=gen_num(10))
    u.save()

print("Created 666 Users.")

c = Competition(name=gen_str(56))
c.save()
competition_idi = int(c.id)
for i in range(competition_amount - 1):
    c = Competition(name=gen_str(random.randint(1,72)))
    c.save()

print("Created 10 Competitions.")
print("user_id: %s , competition_id: %s" % (str(user_idi), str(competition_idi)))

# NOTE: currently the votes functionality is broken as it is just a random number.

competitor_registry = []

cr = Competitor(user_id = User.objects.get(id=random.randint(user_idi ,user_idi + user_amount - 1)).id, \
                competition_id = Competition(id=random.randint(competition_idi, competition_idi + competition_amount - 1)).id, \
                votes = random.randint(0,256) )
cr.save()
competitor_idi = int(cr.id)
competitor_registry.append((cr.user_id, cr.competition_id))
for i in range(competitor_amount - 1):
    random_user_id = random.randint(user_idi ,user_idi + user_amount - 1)
    random_competition_id = random.randint(competition_idi, competition_idi + competition_amount - 1)
    if (random_user_id, random_competition_id) not in competitor_registry:
        cr = Competitor(user_id = User.objects.get(id=random_user_id).id, \
                        competition_id = Competition(id=random_competition_id).id, \
                        votes = random.randint(0,256) )
        cr.save()
    else:
        print("User[%s] is already registered for competition %s" % (str(random_user_id), str(random_competition_id)))

print("Created 200 Competitors.")

# TODO: add a function that will simulate voting between people. Then you can develop the algorithm to return the list of (n) top ranking competitors.


